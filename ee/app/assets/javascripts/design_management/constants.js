// WARNING: replace this with something
// more sensical as per https://gitlab.com/gitlab-org/gitlab/issues/118611
const VALID_DESIGN_FILE_MIMETYPE = 'image/*';

export { VALID_DESIGN_FILE_MIMETYPE as default };
